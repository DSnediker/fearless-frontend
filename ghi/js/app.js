function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
    return `
        <div class="shadow p-3 mb-5 bg-body-tertiary rounded">
        <div class="grid gap-0 row-gap-20">
        <div class="grid gap-0 column-gap-20">
            <div class="card">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
                <p class="card-text">${description}</p>
                </div>
                <div class="card-footer text-body-secondary">
                    ${startDate} - ${endDate}
                </div>
            </div>
        </div>
        </div>
        </div>

    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

    const url = "http://localhost:8000/api/conferences/";

    try {
        const response = await fetch(url);

        if (!response.ok) {
            // figure out what to do if response is bad
            const errorMessage = `
                <div class="alert alert-danger" role="alert">
                    Error getting the data from API
                </div>`;
            const errorContainer = document.createElement("div");
            errorContainer.innerHTML = errorMessage;
            const firstChild = document.body.firstChild;
            document.body.insertBefore(errorContainer, firstChild);
        } else {
            const data = await response.json();

            let i = 0;
            for (let conference of data.conferences){
                const detailURL = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailURL);
                if (detailResponse.ok){
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureURL = details.conference.location.picture_url;
                    const startDate = new Date(details.conference.starts).toLocaleDateString();
                    const endDate = new Date(details.conference.ends).toLocaleDateString();
                    const locationName = details.conference.location.name;
                    const html = createCard(title, description, pictureURL, startDate, endDate, locationName);
                    const columns = document.querySelectorAll('.col');
                    const column = columns[i];
                    column.innerHTML += html;
                    i++;
                    if(i === 3){
                        i = 0;
                    }

                }
            }

            // const conference = data.conferences[0];
            // const nameTag = document.querySelector('.card-title');
            // nameTag.innerHTML = conference.name;

            // const detailUrl = `http://localhost:8000${conference.href}`;
            // const detailResponse = await fetch(detailUrl);
            // if (detailResponse.ok){
            //     const details = await detailResponse.json();
            //     console.log(details);

            //     const description = details.conference.description;

            //     const descriptionTag = document.querySelector('.card-text');
            //     descriptionTag.innerHTML = description;

            //     const imageTag = document.querySelector('.card-img-top');
            //     imageTag.src = details.conference.location.picture_url;
            // }
        }
    } catch (e) {
        //figure out what to do if an error is raised
        console.error(e);
    }

});
